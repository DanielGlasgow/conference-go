import json
import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photos(city, state):
    # test
    # print('key', PEXELS_API_KEY)
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    # print(content)
    try:
        # print({"picture_url": content["photos"][0]["src"]["original"]})
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": f'{city},{state},US',
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"

    response = requests.get(url, params=params)
    content = json.loads(response.content)
#     print(content)
# print(get_weather_data("Austin", "TX"))

    try:
        latitude = content[0]["lat"]
        logitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": logitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"

    response = requests.get(url, params=params)
    content = json.loads(response.content)
#     print(content)
# print(get_weather_data("Austin", "TX"))
    try:
        return {"description": content["weather"][0]["description"],
                "temp": content["main"]["temp"]}
    except (KeyError, IndexError):
        return None
